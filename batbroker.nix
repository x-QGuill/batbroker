{ buildGoModule, zeromq, pkgconfig, gcc }:

buildGoModule {
    name = "batbroker";
    version = "1.2.0";
    src = ./.;
    vendorSha256 = "sha256-CXyn2hIlEQ4u3PAildNz6tHQotOJ3mYZhIgRoQDRckw=";
    nativeBuildInputs = [ pkgconfig gcc ];
    buildInputs = [ zeromq ];

}
