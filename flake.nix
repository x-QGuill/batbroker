{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/22.11";
  };

  outputs = { self, nixpkgs }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };
    in
    {

      packages.${system} = rec {
        batbroker = pkgs.callPackage ./batbroker.nix { };
        default = batbroker;
      };
      
      devShells.${system} = {
        go = pkgs.mkShell {
          buildInputs = [
            pkgs.go  
            pkgs.zeromq
            pkgs.pkgconfig
          ];
        };
      };

    };
}
